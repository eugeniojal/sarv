    <?php $s= 1; ?>
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
       <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">List of Invoices</h1>
            <a href="newinvoice.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i>New Invoice</a>
          </div>
          <!-- Page Heading -->
          
         
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">List of Invoices</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th># Invoice</th>
                      <th>Date</th>
                      <th>Customer</th>
                      <th>Days</th>
                      <th>Total</th>
                      <th>status</th>
                      <th>Option</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th># Invoice</th>
                      <th>Date</th>
                      <th>Customer</th>
                      <th>Days</th>
                      <th>Total</th>
                      <th>status</th>
                      <th>Option</th>
                    </tr>
                  </tfoot>
                  <tbody id="list" >

                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
