<?php 
require_once('../../default/BD.php');
//EJECUTAMOS LA CONSULTA DE BUSQUEDA
$registro = mysqli_query($enlace,"SELECT *FROM listafacturas order by id asc");
//CREAMOS NUESTRA VISTA Y LA DEVOLVEMOS AL AJAX
$fechahoy = new DateTime();
if(mysqli_num_rows($registro)>0){
	while($registro2 = mysqli_fetch_array($registro)){

		$datetime2 = new DateTime($registro2['fecha']);
      $interval = $fechahoy->diff($datetime2);
		echo '

                    <tr>
                      <td>'.$registro2['id'].'</td>
                      <td>'.$registro2['nom_cliente'].'</td>
                      <td>'.$registro2['fecha'].'</td>
                      <td>'.$interval->format('%a días').'</td>
                      <td>'.number_format($registro2['monto'],2,',','.').'</td>
                    
		';
		if($registro2['estatus'] >0){
			echo '<td style="color:green;">Paid</td>
			<td><a onclick="verfact('.$registro2['id'].')"><i class="fas fa-eye"></i></a></td>
			</tr>';

		}else{
			echo '<td style="color:red;">outstanding</td>
			<td><a href="detalle.php">VER</a></td>
			</tr>';
		}
	}
}else{
	echo '<tr>
				<td colspan="6">Agregar Item</td>
			</tr>';
}
?>
