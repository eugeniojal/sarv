<?php
	/* Connect To Database*/
   $s= 1;
	require_once ("default/BD.php");//Contiene funcion que conecta a la base de datos
	$query_perfil=mysqli_query($enlace,"SELECT * from perfil where id=1");
	$rw=mysqli_fetch_assoc($query_perfil);
	$sql=mysqli_query($enlace, "SELECT LAST_INSERT_ID(id) as last from facturas order by id desc limit 0,1 ");
	$rws=mysqli_fetch_array($sql);
	$numero=$rws['last']+1;
?>

<div class="container-fluid  border-bottom-info "  >
    <div class="card shadow mb-4" style="padding: 40px;">
        
       <form class="form-horizontal" role="form" id="datos_factura" method="post">
        <div id="print-area">
                  <div class="row pad-top font-big">
                <div class="col-lg-4 col-md-4 col-sm-4">
                  <a href="" target="_blank">  <img src="../recursos/LOGO_PNG.png" alt="Logo sistemas web" /></a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <strong>E-mail : </strong> <?php echo $rw['email'];?>
                    <br />
                    <strong>Teléfono :</strong> <?php echo $rw['telefono'];?> <br />
					          <strong>Sitio web :</strong> <?php echo $rw['web'];?> 
                   
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <strong><?php echo $rw['nombre_comercial'];?>  </strong>
                    <br />
                    Dirección : <?php echo $rw['direccion'];?> 
                </div>

            </div>
          
            
       
            <div class="row ">
			<hr />
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <h2>Client details :</h2>
                     <select class="cliente form-control" name="cliente" id="cliente" onchange="datos()" >
						<option value="">Selecciona el cliente</option>
						<?php
						 $query = mysqli_query($enlace,"SELECT * FROM clientes");
         				 while ($valores = mysqli_fetch_array($query)) {
            			echo '<option value="'.$valores['id'].'">'.$valores['nombre'].'</option>';
          } ?>
					</select>
                    <span id="direccion"></span>
                    <h4><strong>E-mail: </strong><span id="email"></span></h4>
                    <h4><strong>Teléfono: </strong><span id="telefono"></span></h4>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <h2>Detalles de la factura:</h2>
                    <h4><strong>Factura #: </strong><?php echo $numero;?></h4>
                    <h4><strong>Fecha: </strong> <?php echo date("d/m/Y");?></h4>
				
								
                    <textarea  class="form-control" id="descripcion1" name="descripcion1"   placeholder="Otros comentarios" ></textarea>
                    
                  
                </div>
            </div>
            
         
            <div class="row">
			<hr />
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-striped  table-hover">
                            <thead>
                                <tr>
                                    <th class='text-center'>Item</th>
									<th>Descripción</th>
									<th class='text-center'>Cantidad</th>
                                    <th class='text-right'>Precio unitario</th>
                                    <th class='text-right'>Total</th>
									<th class='text-right'></th>
                                </tr>
                            </thead>
                            <tbody class='items'>
                                
                            </tbody>
                        </table>

                    </div>
                </div>
                
            </div>
            
           
           
            
		
        </div>
       <div class="row"> <hr /></div>
        <div class=" pad-bottom  pull-right">
		
            <div class="col-lg-12 col-md-12 col-sm-12">
                <button type="button" class="btn btn-success " onclick="Save()">Save Invoice</button>
             
              

                
            </div>
        </div>
		</form>
		<br>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        					  <div class="row">
						<div class="col-md-12">
							<label>Descripción del producto/servicio</label>
							<textarea class="form-control" id="descripcion" name="descripcion"  required></textarea>
							<input type="hidden" class="form-control" id="action" name="action"  value="ajax">
						</div>
						
					  </div>

					  <div class="row">
						<div class="col-md-6">
							<label>Cantidad</label>
							<input type="text" class="form-control" id="cantidad" name="cantidad" required>
						</div>
						
						<div class="col-md-6">
							<label>Precio unitario</label>
						  <input type="text" class="form-control" id="precio" name="precio" required>
						</div>
						
					  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="addItem()" >Save changes</button>
      </div>
    </div>
  </div>
</div>
	
   
</div>
  