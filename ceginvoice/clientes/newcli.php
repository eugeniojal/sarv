    <?php $s= 2; ?>
  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Create an Client!</h1>
              </div>
              <form class="user">
                <div class="form-group ">
                  <input type="text" class="form-control form-control-user" id="namecustom" placeholder="Name Customer">
                </div>
                <div class="form-group">
                  <textarea class="form-control form-control-user" id="addresscustom" placeholder="Address"></textarea>
                </div>                
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" id="namecont" placeholder="Name of Contac">
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" id="phonecont" placeholder="Phone">
                  </div>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control form-control-user" id="emailcont" placeholder="Email Address">
                </div>
                <button onclick="GuardarCli()" class="btn btn-primary  btn-block" id="guardar" type="button">
                  Register Client
                </button>
                
 
              </form>
              <hr>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>