
<?php include ('default/header.php'); ?>
<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-img1">
  <div class="col-md-5 p-lg-5 mx-auto my-5 text-light">
    <h2 class="font-weight-small">Sistema Administrativo y de realidad virtual</h2>
    <!-- <p class="lead font-weight-normal">Streamline your billing process and obtain essential reports for the control of your company.</p> -->
    <!-- <a class="btn btn-outline-secondary" href="#">Coming soon</a> -->
  </div>
  <div class="product-device shadow-sm d-none d-md-block"></div>
  <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
</div>



<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
  <h1 class="display-4">Inmuebles</h1>
  <p class="lead">
Los mejores inmuebles a los mejores al mejor precio </p>
</div>


        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                        <img src="recursos/1.jpg" alt="New York" style="height: 100%;width: 100%;">
                        <div class="card-body">
                            <p class="card-text">CASA EN VENTA</p>
                            <p class="card-text"></p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline-secondary">VER INMUEBLE</button>
                                    <!-- <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button> -->
                                </div>
                                <small class="text-muted">9 mins</small>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                        <img src="recursos/1.jpg" alt="New York" style="height: 100%;width: 100%;">
                        <div class="card-body">
                            <p class="card-text">CASA EN VENTA</p>
                            <p class="card-text"></p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline-secondary">VER INMUEBLE</button>
                                    <!-- <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button> -->
                                </div>
                                <small class="text-muted">9 mins</small>
                            </div>
                        </div>
                    </div>
                </div>
                                <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                        <img src="recursos/1.jpg" alt="New York" style="height: 100%;width: 100%;">
                        <div class="card-body">
                            <p class="card-text">CASA EN VENTA</p>
                            <p class="card-text"></p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline-secondary">VER INMUEBLE</button>
                                    <!-- <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button> -->
                                </div>
                                <small class="text-muted">9 mins</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php include ('default/footer.php'); ?>