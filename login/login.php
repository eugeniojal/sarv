<?php 

require_once("default/BD.php");
require_once("classes/Login.php");

$login = new Login();

if ($login->isUserLoggedIn() == false) {
    // the user is logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are logged in" view.
   header("location: php/home.php");

} else {

 ?>
<form method="post" accept-charset="utf-8" action="login.php" name="loginform" autocomplete="on" role="form" class="form-signin">
  <center>
    
    <img class="mb-4" src="recursos/LOGOP.png" alt="" width="120" height="130">
    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
  </center>
  
        <?php
        // show potential errors / feedback (from login object)
        if (isset($login)) {
          if ($login->errors) {
            ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <strong>Error!</strong> 
            
            <?php 
            foreach ($login->errors as $error) {
              echo $error;
            }
            ?>
            </div>
            <?php
          }
          if ($login->messages) {
            ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <strong>Aviso!</strong>
            <?php
            foreach ($login->messages as $message) {
              echo $message;
            }
            ?>
            </div> 
            <?php 
          }
        }
        ?>
  <label for="inputEmail" class="sr-only">Email address</label>
  <input type="email" name="user_name" id="user_name" class="form-control" placeholder="Email address" required autofocus>
  <label for="inputPassword" class="sr-only">Password</label>
  <input type="password" name="user_password" id="user_password" class="form-control" placeholder="Password" required>
  <div class="checkbox mb-3">
    <label>
      <input type="checkbox" value="remember-me"> Remember me
    </label>
  </div>
  <button class="btn btn-lg btn-primary btn-block" type="submit" name="login" id="submit" >Sign in</button>
  <p class="mt-5 mb-3 text-muted">&copy; 2017-<?php echo date('Y');  ?></p>
</form>
<?php } ?>