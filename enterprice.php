<?php include ('default/header.php'); ?>


  

<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-img">
  <div class="col-md-5 p-lg-5 mx-auto my-5 text-light">
    <h1 class="display-0 font-weight-normal">Sistema Administrativo y de realidad virtual</h1>
    <p class="lead font-weight-normal">Creatividad tecnologica.</p>
    <!-- <a class="btn btn-outline-secondary" href="#">Coming soon</a> -->
  </div>
  <div class="product-device shadow-sm d-none d-md-block"></div>
  <div class="product-device product-device-2 shadow-sm d-none d-md-block"></div>
</div>

<div role="main" class="container">
  <div class="row">
    <div class="col-md-12 blog-main">
 

      <div class="blog-post">
        <h2 class="blog-post-title">Sobre el sistema</h2>
        <p class="font-weight-light fp">SA&RV es un sistema administrativo en cual podra hacer peticiones de facturas,presupuestos peticiones de compra o alquiler de inmuebles ademas con la tecnologia de realidad virtual 360° podras visualizar el interior de los inmuebles desde la comodidad del computador.</p>
      </div><!-- /.blog-post -->

  </div><!-- /.row -->

  
</div>
<!-- /.container -->

<?php include ('default/footer.php'); ?>